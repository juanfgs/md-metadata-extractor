require "pp"
module MdMetadata
  class Extractor
    attr_accessor :mode
    def initialize(mode = :hugo)
      # Define input data
      @input = ""
      @result = { :metadata => {} , :content => ""}
      @mode = mode
    end
    def metadata_regex
        /#{Regexp.quote(delimiter)}(.*)#{Regexp.quote(delimiter)}/m
    end
    def content_regex
        /#{Regexp.quote(delimiter)}.*#{Regexp.quote(delimiter)}(.*)\z/m
    end
    def delimiter
      case @mode
      when :hugo
        "+++"
      when :jekyll
        "---"
      end
    end

    def separator
      case @mode
      when :hugo
        "="
      when :jekyll
        ":"
      end
    end
    
    ##
    # Loads an input string for processing, returns a hash of values
    # based on said metadata
    def extract(input)
      raise "Malformed input file" unless metadata_regex.match?(input)
      @metadata = metadata_regex.match(input).to_s.delete(delimiter)
      @metadata.each_line do |line|
        meta = line.split(separator)
        unless meta[1].nil?
          meta[0].strip!
          meta[1].gsub!( '"', ' ').strip!
          @result[:metadata][meta[0]] = meta[1]
        end
      end
      @result[:content] = content_regex.match(input)[-1].to_s.strip!
      @result
    end
  end
end
