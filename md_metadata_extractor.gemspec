# coding: utf-8
Gem::Specification.new do |s|
  s.name = %q{md_metadata_extractor}
  s.version = "0.0.2"
  s.date = %q{2018-11-12}
  s.summary = %q{Metadata extractor for Jekyll/Hugo md files}
  s.authors = ["Juan Francisco Giménez Silva"]
  s.email = "juanfgs@gmail.com"
  s.license = "GPLv3"
  s.files = [
    "Gemfile",
    "Rakefile",
    "lib/md_metadata_extractor.rb",
    "test/md_metadata_extractor_test.rb"
  ]
  s.require_paths = ["lib"]
end
