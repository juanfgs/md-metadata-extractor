require "minitest/autorun"
require "md_metadata_extractor"

class TestExtractor < Minitest::Test
  def setup
    @importer = MdMetadata::Extractor.new
    @testCaseJekyll = <<-MD
        ---
        title : "My Title"
        date : "2014-11-17 19:38:31 +0000 UTC"

        description : "here goes the post description"
        keywords : "here goes the metadata"

        ---
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    MD
    @testCaseHugo = <<-MD
        +++
        title = "My Title"
        date = "2014-11-17 19:38:31 +0000 UTC"
        description = "here goes the post description"
        keywords = "here goes the metadata"
        +++ 
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    MD
    @testCaseBroken = <<-MD
        +-+
        title = "My Title"
        date  "2014-11-17 19:38:31 +0000 UTC" =
        description = "here goes the post description"
        keywords = "here goes the metadata"
        +++ 
        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    MD
  end
  def test_extract
    output = @importer.extract @testCaseHugo
    assert_instance_of Hash, output
  end
  def test_extracts_title_from_hugo_metadata
    output = @importer.extract @testCaseHugo
    assert_equal "My Title", output[:metadata]["title"]
  end
  def test_extracts_title_from_jekyll_metadata
    @importer.mode = :jekyll
    output = @importer.extract @testCaseJekyll
    assert_equal "My Title", output[:metadata]["title"]
  end
  def test_extracts_content_from_hugo_md
    output = @importer.extract @testCaseHugo
    assert_equal "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", output[:content]
  end

  
  def test_raises_exception_on_malformed_file
    assert_raises RuntimeError do
      output = @importer.extract @testCaseBroken
    end
  end
end
